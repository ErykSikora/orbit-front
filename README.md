# orbit-front

## Table of contents
* [General info](#general-info)
* [Technologies](#technologies)
* [Setup](#setup)

## General info
This project is dashboard for a prototype of distributed agent system for university classes.
	
## Technologies
Project is created with:
* [React: 17.0.2](https://pl.reactjs.org/)

## Setup
In order to install all needed dependencies first run:
```
npm install
```
After that run the following command to start the app:
```
npm start
```
The application shoud by default run on http://localhost:3000/
