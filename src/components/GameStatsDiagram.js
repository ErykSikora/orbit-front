import React from 'react';
import { PieChart } from 'react-minimal-pie-chart';
import { green, blue, red } from '../utils/Colors'

import '../css/UserStatsDiagram.css';

const UserStatsDiagram = ({ className, gamesData }) => {

    const chartData = gamesData ? [
        { title: 'Won', value: gamesData.won, color: green },
        { title: 'Tied', value: gamesData.tied, color: blue },
        { title: 'Lost', value: gamesData.lost, color: red },
    ] : [{ title: 'Tied', value: 1, color: blue }]
    
    const formatLabel = (value, total) => Math.round(100*value/total) + "%"

    return (
        <div className={className}>
            <h2>Games stats:</h2>
            <div className="diagram-label" style={{ background: green }}/><p>Won</p>
            <div className="diagram-label" style={{ background: blue }}/><p>Tied</p>
            <div className="diagram-label" style={{ background: red }}/><p>Lost</p>
            <PieChart
                className="diagram-chart"
                data={chartData} 
                lineWidth={10}
                rounded={true}
                labelPosition={75}
                label={({ dataEntry }) => gamesData ? formatLabel(dataEntry.value, gamesData.played) : ""}
                labelStyle={(index) => ({
                    fill: chartData[index].color,
                    fontSize: '5px',
                    fontFamily: 'Lato',
                })} />
        </div>
    );
  }
  
  export default UserStatsDiagram;