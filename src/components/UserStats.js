import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faUserSecret, faGamepad, faHandRock, faHandPaper, faHandScissors } from '@fortawesome/free-solid-svg-icons'

import '../css/UserStats.css';

const UserStats = ({ actor, results, className, setComparedActor }) => {
    return (
        <div className={className} onClick={() => setComparedActor(actor)}>
            <FontAwesomeIcon className="agent-icon" icon={faUserSecret} />
            <div className="player-details">
                <p className={ results ? "player-name" : "player-name gradient" }>{actor.toUpperCase()}_ACTOR</p>
                <p className={ results ? "player-type" : "player-type gradient" }>{actor.toUpperCase()}</p>
            </div>
            <p className="player-games-info">
                <FontAwesomeIcon icon={faGamepad} /> &nbsp;
                Games played: <p className={ results ? "" : " gradient" }>{results ? results.played : "loading..."}</p>
            </p>
            <p className="player-games-info">
                <FontAwesomeIcon icon={faHandRock} /> &nbsp;
                Games won: <p className={ results ? "" : " gradient" }>{results ? results.won : "loading..."}</p>
            </p>
            <p className="player-games-info">
                <FontAwesomeIcon icon={faHandPaper} /> &nbsp;
                Games tied: <p className={ results ? "" : " gradient" }>{results ? results.tied : "loading..."}</p>
            </p>
            <p className="player-games-info">
                <FontAwesomeIcon icon={faHandScissors} /> &nbsp;
                Games lost: <p className={ results ? "" : " gradient" }>{results ? results.lost : "loading..."}</p>
            </p>
        </div>
    );
  }
  
  export default UserStats;