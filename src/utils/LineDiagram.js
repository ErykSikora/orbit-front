const options = {
    scales: {
        yAxes: [
        {
            ticks: {
            beginAtZero: true,
            min: 0,
            max: 100,
            },
        },
        ],
    },
}

const data = {
    datasets: [
        {
            label: 'Won',
            fill: false,
            backgroundColor: '#67b031',
            borderColor: '#67b031',
        },
        {
            label: 'Lost',
            fill: false,
            backgroundColor: '#ff2713',
            borderColor: '#ff2713',
        },
        {
            label: 'Tied',
            fill: false,
            backgroundColor: '#0493cd',
            borderColor: '#0493cd',
        }
    ],
  }

export { options, data }