const green = '#67b031'
const blue = '#0493cd'
const red = '#ff2713'

// const green = 'rgb(0, 130, 30)'
// const blue = 'rgb(5, 55, 205)'
// const red = '#ff2713'

export { green, blue, red }