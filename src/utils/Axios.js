import axios from "axios";

const mAxios = axios.create({
    baseURL: "http://localhost:8001",
    responseType: "json"
});

mAxios.interceptors.request.use(
    (config) => {
        config.headers['Access-Control-Allow-Origin'] = '*';
        return config;
    },
    (error) => Promise.reject(error));

export default mAxios;