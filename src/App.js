import React, { useState, useEffect } from 'react';
import Select from 'react-select'

import mAxios from './utils/Axios'
import Main from './containers/PlayerContainer';

import './App.css';
import logo from "./logo.svg";

const App = () => {
  const [open, setOpen] = useState(false)
  const [actor, setActor] = useState("loading ...")
  const [connected, setConnected] = useState(false);
  
  const [players, setPlayers] = useState([]);
  const [supervisors, setSupervisors] = useState([]);
  const [selectedSupervisor, setSelectedSupervisor] = useState({});

  //TODO To be changed to fetchGames
  const checkConnection = (interval) => {
    mAxios.get(`/`)
      .then(() => {
        setOpen(true)
        if(!connected) setConnected(true)
        clearInterval(interval)
        setActor(players ? players[0].value : "loading ...")
      })
      .catch(() => {
        if(connected) setConnected(false)
      })
    }

  const fetchSupervisors = () => {
    mAxios.get(`/supervisors`)
      .then(response => setSupervisors(response.data.map((supervisor) => {return {value: supervisor["key"], label: supervisor["key"]} })))
      .catch(() => setConnected(false))
  }

  const fetchPlayers = () => {
    mAxios.get(`/supervisors/${selectedSupervisor}/players`)
      .then(response => setPlayers(response.data.map((player) => {return {value: player["key"], label: player["key"]} })))
      .catch(() => setConnected(false))
  }

  useEffect(() => fetchSupervisors(), [])
  useEffect(() => {
    setActor("loading ...")
    fetchPlayers()
  }, [selectedSupervisor])

  useEffect(() => {
    const interval = setInterval(() => checkConnection(interval), 5000)
    return () => clearInterval(interval);
  }, [connected])

  return (
    <div className="page-box">
      <img src={logo} className="App-logo" alt="logo" />
      <div className={ open ? "page-content" : "page-content page-content-closed" }>
        <div className="navigation-bar" onClick={() => setOpen(!open)}>
            <span className="page-title">Dashboard</span>
            <div className="connection-status">
                <div className="connection-status-indicator" style={{ background: (connected ? "#67b031": "#ff2713")}}></div>
                <span className="connection-status-info">{connected ? "CONNECTED" : "DISCONNECTED"}</span>
            </div>
        </div>
        <div className="left-bar">
            <Select defaultValue={supervisors[0]} options={supervisors} onChange={(selected) => { setActor("loading ..."); setSelectedSupervisor(selected.value) }}  />
            <Select defaultValue={players[0]} options={players} onChange={(selected) => setActor(selected.value)}  />
        </div>
        <Main actor={actor} supervisor={selectedSupervisor} setConnected={setConnected}/>
      </div>
    </div>
  );
}

export default App;
