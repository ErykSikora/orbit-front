import React, { useState, useEffect } from 'react';
import { Line } from 'react-chartjs-2';

import mAxios from '../utils/Axios';
import { options, data } from '../utils/LineDiagram'
import UserStats from '../components/UserStats';
import UserStatsDiagram from '../components/GameStatsDiagram';

import '../css/PlayerContainer.css';

const PlayerContainer = ({ actor, supervisor, setConnected }) => {
    const [gamesHistoryData, setGamesHistoryData] = useState(null)
    const [actorGamesResults, setActorGamesResults] = useState(null)
    const [comparedActor, setComparedActor] = useState("random-3")
    const [comparedGamesCount, setComparedGamesCount] = useState(10)
    const [historyInterval, setHistoryInterval] = useState(5)

    const opponentsList = actorGamesResults && Object.keys(actorGamesResults).length !== 0 ? Object.entries(actorGamesResults)
        .map(playerResults => { return <UserStats className="opponent-info" actor={playerResults[0]} results={playerResults[1]} setComparedActor={(actor) => setComparedActor(actor)} /> }) : [
            <UserStats className="opponent-info" actor={"Fake actor"} results={null} setComparedActor={() => {}} />,
            <UserStats className="opponent-info" actor={"Fake actor"} results={null} setComparedActor={() => {}} />,
            <UserStats className="opponent-info" actor={"Fake actor"} results={null} setComparedActor={() => {}} />,
            <UserStats className="opponent-info" actor={"Fake actor"} results={null} setComparedActor={() => {}} />
        ]

    const results = actorGamesResults && Object.keys(actorGamesResults).length !== 0 ? Object.entries(actorGamesResults)
        .map(playerResults => { return {
            played: playerResults[1].played,
            won: playerResults[1].won,
            tied: playerResults[1].tied,
            lost: playerResults[1].lost
        }})
        .reduce((acc, playerResults) => {
            acc.played += playerResults.played
            acc.won += playerResults.won
            acc.tied += playerResults.tied
            acc.lost += playerResults.lost
            return acc
        }) : null

    const setLineDiagramData = () => {
        let won = 0;
        let lost = 0;
        let tied = 0;

        const historyData = gamesHistoryData && gamesHistoryData.history ? gamesHistoryData.history.map((el, index) => {
                won += (el.result === "WON") ? 1 : 0
                return Math.round(1000*(won + gamesHistoryData.startWon)/(index + 1 + gamesHistoryData.startIndex))/10
            }) : []

        const historyDataLost = gamesHistoryData && gamesHistoryData.history ? gamesHistoryData.history.map((el, index) => {
                lost += (el.result === "LOST") ? 1 : 0
                return Math.round(1000*(lost + gamesHistoryData.startLost)/(index + 1 + gamesHistoryData.startIndex))/10
            }) : []

        const historyDataTied = gamesHistoryData && gamesHistoryData.history ? gamesHistoryData.history.map((el, index) => {
                tied += (el.result === "TIE") ? 1 : 0
                return Math.round(1000*(tied + gamesHistoryData.startTied)/(index + 1 + gamesHistoryData.startIndex))/10
            }) : []

        data.labels = historyData.map((_, index) => index + gamesHistoryData.startIndex)
        data.datasets[0].data = historyData
        data.datasets[1].data = historyDataLost
        data.datasets[2].data = historyDataTied
    }

    const handleComparedGamesInput = (event) => {
        if(event.target.validity.valid) {
            setComparedGamesCount(event.target.value)
        }
    }
    const handleComparedGamesIntervalInput = (event) => {
        if(event.target.validity.valid) {
            setHistoryInterval(event.target.value)
        }
    }

    const getOpponentHistory = () => {
        mAxios.get(`/supervisors/${supervisor}/players/${actor}/history/${comparedActor}?count=${comparedGamesCount}`)
            .then(response => {
                setConnected(true)
                setGamesHistoryData(response.data)
            })
            .catch(() => setConnected(false))
    }

    const getPlayerGamesStats = () => {
        mAxios.get(`/supervisors/${supervisor}/players/${actor}/history`)
            .then(response => {
                setConnected(true)
                setActorGamesResults(response.data)
            })
            .catch(() => setConnected(false))
    }

    useEffect(() => {
        getPlayerGamesStats()
        setActorGamesResults(null)
        const interval = setInterval(() => getPlayerGamesStats(), 1000);
        return () => clearInterval(interval);
    }, [actor, supervisor])

    useEffect(() => {
        getOpponentHistory()
        const interval = setInterval(() => getOpponentHistory(), historyInterval * 1000);
        return () => clearInterval(interval);
    }, [actor, supervisor, comparedActor, comparedGamesCount, historyInterval])

    useEffect(setLineDiagramData, [gamesHistoryData])

    return (
        <div className="player-content">
            <UserStats className="player-info" actor={actor} results={results} setComparedActor={() => {}}/>
            <UserStatsDiagram className="player-diagram" gamesData={results} />
            <div className="opponents-list">
                <h2>Opponents stats:</h2>
                { opponentsList }
            </div>
            <div className="opponent-diagram">
                <span>Last games:</span>
                <input className="custom-input" value={comparedGamesCount} onChange={handleComparedGamesInput}  pattern="[0-9]*" />
                <span style={{paddingLeft: 20}}>Fetch interval:</span>
                <input className="custom-input" value={historyInterval} onChange={handleComparedGamesIntervalInput}  pattern="[0-9]*" />
                <button style={{float: "right"}} onClick={() => getOpponentHistory()}>Fetch now</button>
                <Line data={data} options={options} />
            </div>
            <div className="empty-slot"></div>
        </div>
    );
  }
  
  export default PlayerContainer;
  